const fs = require('fs');
const path = require('path');
const readline = require('readline');
const { google } = require('googleapis');

// Caminho para o arquivo de credenciais baixado do Google Cloud Console
const AUTH_CREDENTIALS_PATH = path.join(__dirname, 'credentials.json');
// Caminho onde o token será salvo após a autenticação
const TOKEN_PATH = path.join(__dirname, 'token.json');
// Diretório local que contém os arquivos HTML
const HTML_DIR = path.join(__dirname, '..', '.next/server/pages');

// Função para criar um cliente OAuth2 com as credenciais da aplicação web

async function authenticate() {
  const credentialsData = fs.readFileSync(AUTH_CREDENTIALS_PATH, 'utf8');
  console.log("Dados de Credenciais Carregados:", credentialsData);

  const credentials = JSON.parse(credentialsData);
  console.log("Credenciais Parseadas:", credentials);

  const { client_secret, client_id, redirect_uris } = credentials.web;
  console.log("Redirect URIs:", redirect_uris);

  if (!redirect_uris || redirect_uris.length === 0) {
    throw new Error('Redirect URIs are missing in the credentials file.');
  }

  const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
  // Continua o código...
}


// Função para obter um novo token de autenticação
function getNewToken(oAuth2Client) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: ['https://www.googleapis.com/auth/drive.file'],
  });
  console.log('Autorize este app visitando esta url:', authUrl);
  
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  
  return new Promise((resolve, reject) => {
    rl.question('Digite o código do site aqui: ', (code) => {
      rl.close();
      oAuth2Client.getToken(code, (err, token) => {
        if (err) return reject('Erro ao recuperar token de acesso', err);
        oAuth2Client.setCredentials(token);
        fs.writeFileSync(TOKEN_PATH, JSON.stringify(token));
        resolve(oAuth2Client);
      });
    });
  });
}

// Função para fazer upload dos arquivos HTML para o Google Drive
async function uploadFiles(auth) {
  const drive = google.drive({ version: 'v3', auth });
  const files = fs.readdirSync(HTML_DIR).filter(file => file.endsWith('.html'));

  for (let file of files) {
    const filePath = path.join(HTML_DIR, file);
    const fileMetadata = { name: file };
    const media = {
      mimeType: 'text/html',
      body: fs.createReadStream(filePath),
    };

    const response = await drive.files.create({
      resource: fileMetadata,
      media,
      fields: 'id',
    });
    console.log('Arquivo enviado:', file, 'ID:', response.data.id);
  }
}

// Executar o script de autenticação e upload
(async () => {
  try {
    const auth = await authenticate();
    await uploadFiles(auth);
    console.log('Todos os arquivos foram enviados com sucesso!');
  } catch (error) {
    console.error('Ocorreu um erro:', error);
  }
})();
