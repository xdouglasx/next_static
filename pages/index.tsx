import axios from 'axios';
import { GetStaticProps, InferGetStaticPropsType } from "next";
import Link from "next/link";
import React from 'react';
import styles from '../styles/Home.module.css';

// Definição do tipo Post, se ainda não definido
interface Post {
  id: number;
  title: string;
}

// Página inicial que lista os posts
export default function Home({
  posts,
}: InferGetStaticPropsType<typeof getStaticProps>) {
  return (
    <div className={styles.background}>
      <div className={styles.container}>
        <h1 className="text-2xl font-bold mb-8">Lista de Posts</h1>
        <ul className="list-none">
          {posts.map((post) => (
            <li key={post.id} className="mb-4">
              <Link href={`/${post.id}`} className={styles.button}>
                {post.title}
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

// Função que retorna os posts como props durante o build

export const getStaticProps: GetStaticProps<{ posts: Post[] }> = async () => {
  
  // Fazer chamada à API JSONPlaceholder para obter posts
  const res = await axios.get<Post[]>('https://jsonplaceholder.typicode.com/posts');
  const posts = res.data;

  return {
    props: {
      posts, // Agora `posts` contém os dados da API
    },
  };
};
