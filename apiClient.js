const axios = require('axios');

// Criar uma instância do Axios configurada para a API JSONPlaceholder
const api = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com'
});

// Função para obter posts
async function getPosts() {
    try {
        const response = await api.get('/posts');
        return response.data;
    } catch (error) {
        console.error('Erro ao obter posts:', error);
        throw error;
    }
}

// Exportar as funções que interagem com a API
module.exports = {
    getPosts
};
